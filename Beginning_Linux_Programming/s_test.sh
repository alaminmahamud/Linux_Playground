#!/bin/zsh

echo "Now Let's test the conditions?"

read string1
read string2
read string3

if [ $string1 = $string2 ]; then
    echo "1"
fi

if [ $string1 = $string3 ]; then
    echo "2"
fi

if [[ $string1 == $string2 ]]; then
    echo "3"
fi

[[ $string1 == $string2 ]] && echo "4"
[ $string1 = $string2 ] && echo "5"
