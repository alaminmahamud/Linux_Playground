#!/bin/sh
echo "You are executing script $0"
read timeofday

case "$timeofday" in
    yes | Y | YES | Yes | y )
        echo "Good Morning"
        echo "We should wake up bright early in the morning"
        ;;
    [nN]* )
        echo "Good Afternoon"
        echo "Think what is the effect of your daily activities"
        ;;
    * )
        echo "Can't you say one thing clearly"
        exit 1
esac

exit 0
