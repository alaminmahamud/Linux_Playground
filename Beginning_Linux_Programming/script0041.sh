# expr1 | expr2
expression1 if expr1 is non-zero | otherwise expr2
# expr1 & expr2
Zero if either expression is zero, otherwise expr1
# expr1 = expr2
# expr1 > expr2
# expr1 < expr2
# expr1 != expr2
# expr1 + expr2
# expr1 - expr2
# expr1 * expr2
# expr1 / expr2
# expr1 % expr2


# expr1 | expr2
# expr1 & expr2

# expr1 | expr2
if [ expr1 != 0 ]; then
    expr1
else
    expr2
fi

# expr1 & expr2
if [ expr1 = 0 || expr2 = 0]; then
   0
else
    expr1
fi
