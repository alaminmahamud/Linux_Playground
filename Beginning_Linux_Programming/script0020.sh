#!/bin/sh

rm -rf fred*

echo > fred1
echo > fred2
echo > fred3

mkdir fred4
echo > fred5

for file in fred*
do
    if [ -d $file ]; then
        break
    elif [ -f $file ]; then
        echo $file
    fi
done

echo "first dir encountered $file"
exit 0
