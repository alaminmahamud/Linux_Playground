#!/bin/sh

string1="HELLO"
string2="HELLO"
string3="WORLD"

if [ $string1 = $string2 ]; then
    echo "Y"
fi

if [ $string1 != $string3 ]; then
    echo "N"
fi

exit 0
