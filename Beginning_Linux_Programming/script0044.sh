#!/bin/sh
# printf statement

# The printf command is only available in more recent shells.
# X/Open suggests that we should use it in preference to echo for generating formatted output

# The Syntax:
printf "format string" parameter1 parameter2

\\ - backslash character
\a - alert
\b - backspace character
\f - form feed character
\n - new line character
\r - carriage return character
\t - Tab character
\v - vertical tab character
\000 - The single character with octal value 000.
