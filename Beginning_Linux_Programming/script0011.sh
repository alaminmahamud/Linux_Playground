get_confirm && {
    grep -v "$cdcatnum" $tracks_file > $temp_file
    cat $temp_file > $tracks_file
    echo
    add_record_tracks
}

func1 && {
    command 1
    command 2
    command 3
    func2
}

func1 && {
    command 1
    command 2
    command 3
    func2
}


get_confirm && {
    grep -v "$cdcatnum" $tracks_file > $temp_file
    cat $temp_file > $tracks_file
    echo
    add_record_tracks
}


get_confirm<func1> && {
    command 1
    command 2
    command 3
    <func2>
}

# Statement Blocks
If you want to use multiple statements in a place where only one is allowed, such as in an AND or OR list, you can do so by enclosing them in braces {} to make a statement block. For Example, in the application presented later in this chapter, you\'ll see the following code:

get_confirm && {
    grep -V "$cdcatnum" $tracks_file > $temp_file
    cat $temp_file > $tracks_file
    echo
    add_record_tracks
}

get_confirm && {
    grep -V "$cdcatnum" $tracks_file > $temp_file
    cat $temp_file > $tracks_file
    echo
    add_recor_tracks
}
