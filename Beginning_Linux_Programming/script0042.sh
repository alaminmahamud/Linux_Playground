#!/bin/sh
# expr1 | expr2
expr1 if expr1 is non-zero, otherwise expr2
# expr1 & expr2
zero if either expression is zero, otherwise expr1
