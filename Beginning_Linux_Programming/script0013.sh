#!/bin/sh

foo_bar_func () {
    echo "This function has the name of foo_bar_func"
    echo "It has three parts"
    echo "The first -> foo"
    echo "The second -> bar"
    echo "The third -> func"
}

echo "Script Starting to Execute"

read permission

case $permission in
    [Yy] )
        echo "Permission is granted"
        ;;
    [Nn] )
        echo "Permission is denied"
        exit 1
        ;;
    *    )
        echo "Try Again with Only Yy | Nn"
        ;;
esac

echo "Script is executing"
echo $foo_bar_func
echo "Script is ended"
