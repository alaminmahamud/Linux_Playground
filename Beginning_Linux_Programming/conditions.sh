#!/bin/sh

# -n string :: True if the string is not null
# -z string :: True if the string is null (an empty string)

# -n string :: True if the string is not null
# -z string :: True if the string is null [an empty string]

# -n string :: True if the string is not null
# -z string :: True if the string is null (an empty string)

# -n string :: True if the string is not null
# -z string :: True if the string is null [an empty string]

# -n string :: True if the string is not null
# -z string :: True if the string is null (an empty string)

# -n string :: True if the string is not null
# -z string :: True if the string is null (an empty string)

# $string1=""; means that the empty string is assigned to $string1.
# In this case, $string1.length() is the same as "".length(), which will yield 0 as expected.
# $string2=null; means that (NULL) or "no value at all" is assigned to $string2. So this one, $string2.length() is the same as null.length(), which will yield a NullPointerException as you can't call methods on NULL variables (pointers, sort of) in Java.

# -n string :: True if the string is not null.
# -z string :: True if the string is null [an empty string.]
# -n string :: True if the string is not null.
# -z string :: True if the string is null [an empty string.]

# Arithmetic Comparison

#expression1 -eq expression2
#expression1 -eq expression2
#expression1 -eq expression2
#expression1 -eq expression2
#expression1 -eq expression2
#expression1 -eq expression2

# True if the expressions are equal
# True if the expressions are not equal
# True if expression1 is greater than expression2
# True if expression1 is greater than expression2
# True if expresison1 is greater than or equal to expression2
# True if expression1 is greater than or equal to expression2
# True if expression1 is greater than or equal to expression2
# True if expression1 is greater than or equal to expression2
# True if expression1 is greater than or equal to expression2
# True if expression1 is greater than or equal to expression2
# True if expression1 is greater than or equal to expression2

# -d file => True if the file is a directory
# -e file => True if the file exists
# -f file => True if the file is a regular file.
# -g file => True if set-group-id is set on file.
# -r file => True if the file is readable.
# -s file => True if the file has non-zero size.
# -u file => True if set-user-id is set on file.
# -w file => True if the file is writeable
# -x file => True if the file is executable.


# -d file => True if the file is a directory.
# -e file => True if the file exists.
# -f file => True if the file is a regular file.
# -g file => True if set-group-id is set on file.
# -r file => True if the file is readable
# -s file => True if the file has non-zero file.
# -u file => True if set-user-id is set on file.
# -w file => True if the file is writeable
# -x file => True if the file is executable.


# If
# The if statements is very simple. It tests the result of a command and then conditionally executes a group of statements.
