#!/bin/sh
# return
The return command causes function to return. We mentioned this when we looked at functions earlier.
Return takes a single numeric parameter which is available to the script calling the fuction. If no parameter is specified, return defaults to the exit code of the last command.x
