#!/bin/sh

# first.sh
# This file looks all the files in the directory
# and look for POSIX
# then output those matching files with more command


for file in *
do
    if grep -l POSIX $file
    then
        more $file
    fi
done

exit 0
