#!/bin/sh

foo=1

while [ $foo -le 20 ]; then
do
    echo $foo
    foo=$(($foo+1))
done
