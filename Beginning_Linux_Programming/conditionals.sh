#!/bin/sh

echo "Is it morning? Please answer yes or no"
read timeofday

if [ $timeofday = "yes" ]; then
    echo "Good Morning"
else
    echo "Good Afternoon"
fi

exit 0
