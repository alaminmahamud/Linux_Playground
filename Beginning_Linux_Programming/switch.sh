#!/bin/sh

echo "Is it morning? Please answer yes or no"
read timeofday

case "$timeofday" in
    yes) echo "Good Morning";;
    no ) echo "Good Afternoon" ;;
    y  ) echo "Good Morning By Shorthand - y";;
    n  ) echo "Good Afternoon By Shorthand -n";;
    *  ) echo "Time is not recognized";;
esac
exit 0


