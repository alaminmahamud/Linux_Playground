#!/bin/sh
echo "Is it morning? Please answer yes or no"
read timeofday

case $timeofday in
    y | Y | yes | YES | Yes | YEs | YeS ) echo "Good Morning";;
    n | N | no  | No  | nO  | NO        ) echo "Good Afternoon";;
    *                                   ) echo "NOT RECOGNIZABLE";;
esac

exit 0
