#!/bin/sh

: ${FOO:=bar}
: ${FOO:=bar}
: ${FOO:=bar}
: ${FOO:=bar}
: ${FOO:=bar}

# sets foo = bar if foo is unset

: ${FOO:=bar}

: ${FOO:=bar}
: ${FOO:=bar}
: ${FOO:=bar}
: ${FOO:=bar}
: ${FOO:=bar}

# sets foo=bar if foo is unset

: ${FOO:=bar}
