#!/bin/sh
yes_or_no () {
    echo "Is your name $* ?"
    while true
    do
        echo -n "Enter Yes Or No: "
        read x
        case $x in
            [Yy] | [Yy][Ee][Ss] )
                return 0
                ;;
            [Nn] | [Nn][Oo]     )
                return 1
                ;;
            *                   )
                echo "Answer Yes Or No Only!"
                ;;
        esac
    done
}

echo "Original Parameters are $*"

if yes_or_no "$*"
then
    echo "Hi $1, nice name"
else
    echo "Never Mind"
fi

exit 0

