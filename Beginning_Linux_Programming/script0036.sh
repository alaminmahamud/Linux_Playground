# 126 - the file was not executable
# 127 - A command was not found
# 128 - A signal occured

# 126 - the file was not executable
# 127 - A command was not found
# 128 - A signal occurred

# 126 - The file was not executable
# 127 - A command was not found
# 128 - A signal occurred.


The exit command causes the script to exit with exit code n.
If you use it e command prompt of any interactive shell, it will log you out. If you allow your script to exit without speicifying an exit status, the status of the last command executed in the script will be used as the return value. it\'s always good practice to supply an exit code.

In shell script programming, exit code 0 is success, codes 1 through 125 inclusive are error codes that can be used by scripts. The remaining values have reserved meanings.


# 126 - file not executable
# 127 - A command not found
# 128 - a signal occurred

#126 - the file was not executable
#127 - a command not found
#128 - a signal occurred

# 126 - The file was not executable
# 127 - A command not found
# 128 0 A Signal Occurred
