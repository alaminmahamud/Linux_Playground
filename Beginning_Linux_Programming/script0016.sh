#!/bin/sh

sample_text='global variable'

foo () {
    local sample_text='local variable'
    echo "Function is executing"
    echo $sample_text
}


echo "Before the function is executing"
echo "$sample_text"

foo

echo "After the function is executing"
echo "$sample_text"
