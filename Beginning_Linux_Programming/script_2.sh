#!/bin/sh
echo "\$0 means the name of the script"
echo $0

echo "\$# means number of parameters passed to the script"
echo $#

echo "\$1 means the first parameter"
echo $1

echo "\$2 means the second parameter"
echo $2

echo "\$3 means the third parameter"
echo $3


echo "\$* means A list of all the parameters, in a single variable, separated by the first character in the environment variable IFS"
echo "$*"

echo "\$@ means A subtle version of \$*, that doesn't use the IFS environment variable"
echo "$@"

unset IFS

echo "$*"
echo "$@"

IFS='|'
echo "$*"
echo "$@"
