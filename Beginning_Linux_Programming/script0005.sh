#!/bin/sh

echo "Is it morning? "
read timeofday

case $timeofday in
    [Yy] | [Yy][Ee][Ss] )
        echo "Morning"
        echo "Wow!"
        echo "Sunrise"
        ;;
    [Nn]*)
        echo "Afternoon"
        ;;
    *)
        echo "EXCEPT"
        exit 1
        ;;
esac

exit 0
