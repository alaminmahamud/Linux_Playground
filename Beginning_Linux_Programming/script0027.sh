#!/bin/sh
rm -rf fred*

echo > fred1
echo > fred2
echo > fred3
mkdir fred4
echo > fred5

for file in fred*
do
    if [ -d $file ]; then
        echo "skipping $file"
        continue;
    fi
    echo $file
done

rm -rf fred*
exit 0
