#!/bin/sh

echo > fred1
echo > fred2
echo > fred3
mkdir fred4
echo > fred5

for file in fred*
do
    if [ -d $file ]; then
        echo "Skipping directory $file"
        continue;
    fi
    echo file is $file
done
exit 0
