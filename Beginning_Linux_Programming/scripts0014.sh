#!/bin/sh

# When a function is invoked, the positional parameters to the script, $*, $@ $#, $0, $1, $2

# and so on are replaced by the parameters to the function. That's how you read the parameters passed to the function. When the function finishes, they are restored to their previous values.


# IMPORTANT: #
foo () {
    echo  JAY
}
result="$(foo)"
echo $result
