#!/bin/sh
trap 'rm -rf /tmp/my_tmp_file_$$' INT
echo Creating file /tmp/my_tmp_file_$$
date > /tmp/my_tmp_file_$$

## WHILE LOOP

while [ -f /tmp/my_tmp_file_$$ ]; do
    echo "File Exists"
    sleep 1
done
echo The file no longer exists

trap - INT
echo creating file /tmp/my_tmp_file_$$
date > /tmp/my_tmp_file_$$

echo "Press interrupt (Control-C) to interrupt"
while [ -f /tmp/my_tmp_file_$$ ]; do
    echo File Exists
    sleep 1
done

echo We never get here
exit 0
