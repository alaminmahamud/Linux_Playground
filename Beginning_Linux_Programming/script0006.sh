#!/bin/sh

if [ -f ./THIS_FILE.txt ]; then
    foo="TRUE"
elif [ -f ./THE_OTHER_FILE.txt ]; then
    foo="TRUE"
elif [ -f ./THAT_FILE.txt ]; then
    foo="TRUE"
else
    foo="FALSE"
fi

if [ $foo = "FALSE" ]; then
    echo "ONE OF THE FILE EXISTS"
elif [ $foo = "TRUE" ]; then
    echo "O"
fi
